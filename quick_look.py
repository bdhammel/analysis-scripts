import os
import sys
import matplotlib.pyplot as plt

SAVE_PATH = "/Users/bdhammel/Documents/NTF/Analysis/2015_6_bs/quicklook/"  
CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cplot
from analysis import xray_diag
from analysis import anode_bdot
from analysis import ekspla

import faraday_cups
import liquida
import dores
import diode_array
import streak_window

def fc(path, save_path):
    faraday_cups.get_fc_signals(path)
    shotnumber, signals = faraday_cups.get_fc_signals(path)
    faraday_cups.plot_fc(shotnumber, signals)
    plt.savefig('{path}/fc.png'.format(path=save_path))
    plt.close('all')

def xrays(path, save_path):
    xray_diag.get_xrays(path)
    plt.savefig('{path}/5ch.png'.format(path=save_path))

    pcd = cplot.get_line_from_plot('PCD#6.4')
    sb = cplot.get_line_from_plot('stack')
    plt.close('all')

    tds10 = liquida.load_scope(path, 'TDS10', 'DIAG')
    liquida.load_additional_delays(tds10)
    liquida.plot_scope(tds10)
    si = cplot.get_line_from_plot('Si')
    la = cplot.get_line_from_plot('PMT1')
    la.set_label('Liquid A')
    plt.close('all')
    
    dores_scope = dores.dores(path)
    dores.graph_dores(dores_scope)
    dores_sig = cplot.get_line_from_plot('PMT1')
    dores_sig.set_label('DOReS')
    plt.grid()
    plt.close('all')

    fig = plt.figure('Xrays', figsize=(8.5,11))
    fig.subplots_adjust(hspace=0)

    ax1 = fig.add_subplot(511)
    cplot.append_line(sb)
    ax1.set_ylabel('B-dot')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.title('Xrays ({})'.format(shotnumber))
    plt.grid()
    ax1.legend_.remove()

    ax2 = fig.add_subplot(512, sharex=ax1)
    cplot.append_line(pcd)
    ax2.set_ylabel('PCD#6')
    plt.setp(ax2.get_xticklabels(), visible=False)
    plt.grid()
    ax2.legend_.remove()

    ax3 = fig.add_subplot(513, sharex=ax1)
    cplot.append_line(si)
    ax3.set_ylabel('Si')
    plt.setp(ax3.get_xticklabels(), visible=False)
    plt.grid()
    ax3.legend_.remove()

    ax4 = fig.add_subplot(514, sharex=ax1)
    cplot.append_line(la)
    ax4.legend_.remove()
    plt.setp(ax4.get_xticklabels(), visible=False)
    plt.grid()
    ax4.set_ylabel('Liquid A')

    ax5 = fig.add_subplot(515, sharex=ax1)
    cplot.append_line(dores_sig)
    ax5.legend_.remove()
    plt.grid()
    ax5.set_ylabel('DOReS')

    plt.xlim(-50,300)

    plt.savefig('{path}/xray.png'.format(path=save_path))
    plt.close('all')

def get_ref(path):
    shotnumn, tds12 = cscopes.get_scope(path, 'TDS12', 'DIAG')
    fig = plt.figure('reflectivity')
    sb = tds12.get_channel('Ch. 4')
    ref = tds12.get_channel('Ch. 3')
    ax1 = fig.add_subplot(111)
    ax2 = ax1.twinx()

    ax1.plot(sb.time(), sb.signal())
    plt.ylim(-6,10)
    ax2.plot(ref.time(), ref.signal(), color='g')
    plt.ylim(-2.5,5)

    cplot.align_yaxis()

    plt.title('Reflectivity')
    plt.grid(axis=u'x')
    plt.xlim(-50,300)
    plt.savefig('{path}/ref.png'.format(path=save_path))

def get_sweep(path):

    shotnumn, tds12 = cscopes.get_scope(path, 'TDS12', 'DIAG')
    
    streak_window.get_streak_window(tds12, 'Ch. 1')
    plt.savefig('{path}/sweep.png'.format(path=save_path))

if __name__ == "__main__":
    reload(xray_diag)
    reload(anode_bdot)
    reload(dores)
    reload(ekspla)
    reload(liquida)
    reload(faraday_cups)
    reload(diode_array)

    plt.ion()
    path = cscopes.prompt_for_path()
    shotnumber = os.path.basename(path)

    save_path = SAVE_PATH + shotnumber
    if not os.path.exists(save_path):
        os.makedirs(save_path)


    print "\nloading save-to directory as: " + save_path

    #fc(path, save_path)
    xrays(path, save_path)
    anode_bdot.get_current(path)
    plt.savefig('{path}/current.png'.format(path=save_path))
    plt.close('all')
    #ekspla.get_ekspla(path, delay=None)
    #plt.savefig('{path}/ekspla.png'.format(path=save_path))
    #plt.close('all')

    """
    diode_array.analyze_array(path)
    plt.savefig('{path}/diode_array.png'.format(path=save_path))
    plt.close('all')
    """
    get_ref(path)
    plt.close('all')
    get_sweep(path)
    plt.close('all')



