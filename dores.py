import matplotlib.pyplot as plt
import sys

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes, cplot, cmath

SCOPE_NAME = 'TDS2'
SCOPE_TYPE = 'DIAG'
CHANNEL = 'Ch. 1'
SCOPE_FULLNAME = '_'.join([SCOPE_NAME, SCOPE_TYPE])
TIMING_FILE = '/Users/bdhammel/Documents/research_programming/core_diag/timing/cable_delays.json'
#'/Users/bdhammel/Documents/NTF/Analysis/2014_5_Jet/cable_delays.json'

def dores(shot_path):
    TDS3 = cscopes.import_scope_preferences('TDS3_DIAG')
    shotnumber, tds3_raw_data = cscopes.read_scope_from_directory(shot_path, 'TDS3', 'DIAG')
    TDS3.load_data(tds3_raw_data)
    dores = cscopes.import_scope_preferences(SCOPE_FULLNAME, TIMING_FILE)
    shotnumber, raw_data = cscopes.read_scope_from_directory(shot_path, SCOPE_NAME, SCOPE_TYPE) 
    dores.load_data(raw_data)
    dores.set_trigger_to_external_scope(TDS3)
    return dores

def graph_dores(dores):
    hxr = dores.get_channel(CHANNEL)
    bdot = dores.get_channel('Ch. 4')
    plt.plot(hxr.time(), hxr.signal(), label=hxr.verbose_name)

    plt.xlim(100,500)
    plt.legend()
    plt.draw()


if __name__ is "__main__":
    plt.ion()
    print '''
    #======================================================================#
    #   Dores
    #======================================================================#
    
    
    '''
    plt.ion()
    plt.close('all')
    print "\nload shot directory"
    shot_path = cscopes.prompt_for_path()
    dores = dores(shot_path)
    graph_dores(dores)
    ax = plt.gca()
    line = ax.lines[0]
    hl = cmath.peak_integrator()
