import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag/"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cmath
from core import cplot

LA_TOF = 10
LA_INTERNAL_DELAY = 7
SI_TOF = 3
CHANNEL = 'Ch. 1'


def load_scope(path, scopename, scopetype):
    TDS3 = cscopes.import_scope_preferences('TDS3_DIAG')
    shotnumber, tds3_raw_data = cscopes.read_scope_from_directory(path, 'TDS3', 'DIAG')
    TDS3.load_data(tds3_raw_data)

    scope = cscopes.import_scope_preferences(scopename + "_" + scopetype)
    shotnumber, raw_data = cscopes.read_scope_from_directory(path, scopename, scopetype)
    scope.load_data(raw_data)
    #scope.set_trigger_to_external_scope(TDS3)

    return scope

def plot_scope(scope):
    ch1 = scope.get_channel('Ch. 1')
    ch2 = scope.get_channel('Ch. 2')
    ch3 = scope.get_channel('Ch. 3')
    ch4 = scope.get_channel('Ch. 4')
    ch = scope.get_channel(CHANNEL)
    plt.figure(scope.name)
    plt.plot(ch1.time(), ch1.signal(), label=ch1.verbose_name)
    plt.plot(ch2.time(), ch2.signal(), label=ch2.verbose_name)
    plt.plot(ch3.time(), ch3.signal(), label=ch3.verbose_name)
    plt.plot(ch4.time(), ch4.signal(), label=ch4.verbose_name)
    #plt.plot(ch.time(), ch.signal(), label=ch.verbose_name)
    plt.ylim(-25,5)
    cplot.plot_on_differnt_y('SB')
    plt.xlim(-50,300)
    cplot.align_yaxis()
    plt.legend(loc=4)
    plt.title(scope.name)
    plt.show()           

def load_additional_delays(scope):
    ch1 = scope.get_channel('Ch. 1')
    ch2 = scope.get_channel('Ch. 2')
    ch3 = scope.get_channel('Ch. 3')
    ch1.additional_delay(LA_TOF+LA_INTERNAL_DELAY+5)
    ch2.additional_delay(LA_TOF+LA_INTERNAL_DELAY+5)
    ch3.additional_delay(SI_TOF)

if __name__ == '__main__':

    plt.ion()
    plt.close('all')
    path = cscopes.prompt_for_path()
    tds10 = load_scope(path, 'TDS10', 'DIAG')
    load_additional_delays(tds10)
    plot_scope(tds10)
    ax = plt.gca()
    line = ax.lines[0]
    #hl = cmath.peak_integrator()

    """
    x, y = map(np.array, zip(*line.get_xydata()))
    t = x[(x > 0) & (x < 200)]
    s = y[(x > 0) & (x < 200)]

    fill = ax.fill_between(
                    t, 
                    s,
                    0,
                    facecolor='red', 
                    alpha=0.5
                )
    plt.draw()

    dx = t[0]-t[-1],
    min = s.min(),
    max = s.max(),
    tmin = t[np.where(s==min)][0],
    tmax = t[np.where(s==max)][0],
    area = np.trapz(s)

    print '''
    time at max: {tmax}
    max y:       {max}
    area:        {area}
    '''.format(
            max=max,
            tmax=tmax,
            area=area
            ),

    ty = np.array(s)
    low_bar = np.array(ty)
    high_bar = np.array(ty)

    for i in range(1, len(ty)-1):
        if ty[i] < ty[i-1] and ty[i] < ty[i+1]:
            low_bar[i] = np.average([ty[i-1],ty[i+1]])
        elif ty[i] > ty[i-1] and ty[i] > ty[i+1]:
            high_bar[i] = np.average([ty[i-1],ty[i+1]])

    plt.plot(t, low_bar, '--g')
    plt.plot(t, high_bar, '--r')
    plt.draw()

    low_area = np.trapz(low_bar)
    high_area = np.trapz(high_bar)

    print '''
    area +/-:        {low_area}  {high_area}
    '''.format(
            low_area=low_area,
            high_area=high_area
            )
    """
