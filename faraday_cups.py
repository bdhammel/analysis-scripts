import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag/"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cmath
from core import cplot

def stack_bdot_to_current(time, voltage):
    """Integrate the stack Bdot signal

    No calibration factor is know for the stack bdot. However, it is useful to 
    compare to for timing purposes

    Args:
        time (array)
        voltage (array)

    Returns:
        t (array)
        I (array)
        
    """
    voltage -= voltage[:200].mean()
    t, I = cmath.integrate_signal(time, voltage)
    return t, I/I.max()

def analyze(lines):

    line = lines[0]

    x, y = map(np.array, zip(*line.get_xydata()))
    t = x[(x > 0) & (x < 150)]
    s = y[(x > 0) & (x < 150)]

    ax = line.axes

    fill = ax.fill_between(
                    t, 
                    s,
                    0,
                    facecolor='red', 
                    alpha=0.5
                )
    plt.draw()

    dx = t[0]-t[-1],
    min = s.min(),
    max = s.max(),
    tmin = t[np.where(s==min)][0],
    tmax = t[np.where(s==max)][0],
    area = np.trapz(s[s<0])

    print '''
    time at max: {tmax}
    max y:       {max}
    area:        {area}
    '''.format(
            max=max,
            tmax=tmax,
            area=area
            )

    ty = np.array(s)
    low_bar = np.array(ty)
    high_bar = np.array(ty)

    for i in range(1, len(ty)-1):
        if ty[i] < ty[i-1] and ty[i] < ty[i+1]:
            low_bar[i] = np.average([ty[i-1],ty[i+1]])
        elif ty[i] > ty[i-1] and ty[i] > ty[i+1]:
            high_bar[i] = np.average([ty[i-1],ty[i+1]])

    plt.plot(t, low_bar, '--g')
    plt.plot(t, high_bar, '--r')
    plt.draw()

    low_area = np.trapz(low_bar[s<0])
    high_area = np.trapz(high_bar[s<0])

    print '''
    area +/-:        {low_area}  {high_area}
    '''.format(
            low_area=low_area,
            high_area=high_area
            )

    return area

def plot_fc(shotnumber, signals):
    fig = plt.figure('Faraday Cups', figsize=(8.5,11))
    fig.subplots_adjust(hspace=0)

    total_area = 0
    st, si = stack_bdot_to_current(signals['sb'].time(), signals['sb'].signal())
    ax1 = fig.add_subplot(511)
    line = ax1.plot(st, si)
    ax1.set_ylabel('Current')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.grid()


    plt.title('Faraday Cups ({})'.format(shotnumber))

    ax2 = fig.add_subplot(512, sharex=ax1)
    line = ax2.plot(signals['fc8'].time(), signals['fc8'].signal())
    ax2.set_ylabel('fc8')
    plt.grid()
    plt.setp(ax2.get_xticklabels(), visible=False)

    print "FC8"
    print "*"*80
    #total_area += analyze(line)

    ax3 = fig.add_subplot(513, sharex=ax1, sharey=ax2)
    line = ax3.plot(signals['fc9'].time(), signals['fc9'].signal())
    ax3.set_ylabel('fc9')
    plt.grid()
    plt.setp(ax3.get_xticklabels(), visible=False)

    print "FC9"
    print "*"*80
    #total_area += analyze(line)

    ax4 = fig.add_subplot(514, sharex=ax1, sharey=ax2)
    line = ax4.plot(signals['fc10'].time(), signals['fc10'].signal())
    ax4.set_ylabel('fc10')
    plt.grid()
    plt.setp(ax4.get_xticklabels(), visible=False)

    print "FC10"
    print "*"*80
    #total_area += analyze(line)

    ax5 = fig.add_subplot(515, sharex=ax1, sharey=ax2)
    line = ax5.plot(signals['fc11'].time(), signals['fc11'].signal())
    ax5.set_ylabel('fc11')
    plt.grid()
    #plt.setp(ax5.get_xticklabels(), visible=False)

    print "FC11"
    print "*"*80
    #total_area += analyze(line)

    """
    ax6 = fig.add_subplot(616, sharex=ax1)
    ax6.plot(signals['fc12'].time(), signals['fc12'].signal())
    plt.grid()
    ax6.set_ylabel('fc12')
    """

    plt.xlim(-55,250)
    ax1.set_ylim(-.1,1.1)
    ax5.set_ylim(top=5)
    plt.xlabel("Time [ns]")


def get_fc_signals(path):
    shotnumber, tds3 = cscopes.get_scope(path, 'TDS3', 'DIAG')
    shotnumber, tds7 = cscopes.get_scope(path, 'TDS7', 'DIAG')
    #tds7.set_trigger_to_external_scope(tds3)
    shotnumber, tds6 = cscopes.get_scope(path, 'TDS6', 'DIAG')
    #tds6.set_trigger_to_external_scope(tds7)
    sb = tds7.get_channel('Ch. 4')
    fc8 = tds7.get_channel('Ch. 2')
    fc9 = tds7.get_channel('Ch. 3')
    fc10 = tds6.get_channel('Ch. 1')
    fc11 = tds6.get_channel('Ch. 2')
    fc12 = tds6.get_channel('Ch. 3')

    return shotnumber, {
                        'sb':sb,
                        'fc8':fc8,
                        'fc9':fc9,
                        'fc10':fc10,
                        'fc11':fc11,
                        'fc12':fc12
                        }  


if __name__ == '__main__':
    plt.close('all')
    plt.ion()
    path = cscopes.prompt_for_path()
    shotnumber, signals = get_fc_signals(path)
    plot_fc(shotnumber, signals)
    plt.savefig("/Users/bdhammel/Desktop/erik/{}.png".format(shotnumber))
    

