import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag/"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cmath
from core import cplot

SCOPENAME = "TDS12"
SCOPETYPE = "DIAG"
REF_CH = "Ch. 3"

def stack_bdot_to_current(time, voltage):
    """Integrate the stack Bdot signal

    No calibration factor is know for the stack bdot. However, it is useful to 
    compare to for timing purposes

    Args:
        time (array)
        voltage (array)

    Returns:
        t (array)
        I (array)
        
    """
    voltage -= voltage[:200].mean()
    t, I = cmath.integrate_signal(time, voltage)
    return t, I/I.max()

def load_scope(path, scopename=SCOPENAME, scopetype=SCOPETYPE):

    scope = cscopes.import_scope_preferences(scopename + "_" + scopetype)
    shotnumber, raw_data = cscopes.read_scope_from_directory(path, scopename, scopetype)
    scope.load_data(raw_data)

    return scope

def get_reflectivity(scope, ref_ch=REF_CH):
    trigger = scope.get_channel(scope.trigger_channel)
    scope.set_trigger_ch(trigger, trigger_level=.5) # for the piece of shit scopes
    stack = scope.get_channel(scope.trigger_channel)
    streak = scope.get_channel(ref_ch)
    time, current = stack_bdot_to_current(stack.time(), stack.signal())
    plt.figure('Reflectivity')
    plt.plot(time, current, label='current')
    s = streak.signal(normalize=True)
    s/=s[:300].mean()
    plt.plot(streak.time(), s, picker=5, label='reflectivity')
    plt.xlim(-40,400)
    plt.ylim(-.05, 1.2)
    plt.legend()
    plt.xlabel('Time [ns]')
    plt.ylabel('Arbitrary units')
    plt.draw()


if __name__ == "__main__":
    plt.ion()
    print '''
    #==========================================================================#
    #   Get the timing of the streak window
    #==========================================================================#
    

    '''
    plt.close('all')
    plt.ion()

    path = cscopes.prompt_for_path()
    scope = load_scope(path)
    get_reflectivity(scope)
    

