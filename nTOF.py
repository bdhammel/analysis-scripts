import numpy as np
import matplotlib.pyplot as plt
import sys
import csv


SAVE_PATH = "/Users/bdhammel/Dropbox/Gas_Puff_Hafiz/4_2015/quicklook/"  
# SAVE_PATH = "/Users/bdhammel/Documents/NTF/Analysis/2014_5_Jet/shots/"

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

import core_diag as core
import streak_window
import xray_diag

TOF = 15
CABLE_DELAY = 313
STACK_DELAY = 33.2 + 1.5 + 52.5 + 10 + 15 + 5 + 5

DELAY = CABLE_DELAY + TOF - STACK_DELAY

def nTOF_data():
    readin = False
    time = []
    data = []
    print("load nTOF path")
    path = core.prompt_for_path()
    with open (path, 'U') as csvfile:
        reader = csv.reader(csvfile, delimiter = ',' )
        
        for row in reader:
            if readin:
                time.append(float(row[0])*10**9 - DELAY)
                data.append(float(row[1]))
            if row[0] == 'Time' and not readin:
                print 'reading in'
                readin = True

    data = np.array(data)
    data /= -1*data.min()

    ax = plt.gca()
    ax.plot(time, data, 'g')
    plt.draw()
    return {'time':time, 'signal':data}

def pcd_data(path):
    plt.close('all')
    xray_diag.get_xrays(path)
    bdot_trace = core.get_line_from_plot('stack')
    xrd_trace = core.get_line_from_plot('PCD_1')
    bdot_sig = bdot_trace.get_ydata()
    bdot_sig /= bdot_sig.max()
    return (
        {'time':bdot_trace.get_xdata(), 'signal':bdot_sig},
        {'time':xrd_trace.get_xdata(), 'signal':xrd_trace.get_ydata()}
        )
    

def streak_data(path):
    plt.close('all')
    streak_window.get_streak_window(path)
    current_trace = core.get_line_from_plot('current')
    streak_trace = core.get_line_from_plot('streak')
    return (
        {'time':current_trace.get_xdata(), 'signal':current_trace.get_ydata()},
        {'time':streak_trace.get_xdata(), 'signal':streak_trace.get_ydata()}
        )

def plot_data(data):
    plt.close('all')
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax1.plot(data['current']['time'], data['current']['signal'], label='current')
    ax1.plot(data['streak']['time'], data['streak']['signal'], label='streak')
    ax2.plot(data['stack']['time'], data['stack']['signal'], label='stack')
    ax2.plot(data['xrd']['time'], data['xrd']['signal'], label='xrd')
    ax2.plot(data['nTOF']['time'], data['nTOF']['signal'], label='nTOF')
    ax1.set_xlim(-300,700)
    ax2.set_xlim(-300,700)
    plt.draw()

if __name__ == "__main__":
    plt.close('all')
    plt.ion()
    path = core.prompt_for_path()
    data = {}
    data['current'], data['streak'] = streak_data(path)
    data['nTOF'] = nTOF_data()
    data['stack'], data['xrd'] = pcd_data(path)
    plot_data(data)



