import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag/"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cmath
from core import cplot


def load_scope(path, scopename, scopetype):

    scope = cscopes.import_scope_preferences(scopename + "_" + scopetype)
    shotnumber, raw_data = cscopes.read_scope_from_directory(path, scopename, scopetype)
    scope.load_data(raw_data)

    return scope

def plot_scope(scope, shotnumber):


    d1 = scope.get_channel('Ch. 14')
    d2 = scope.get_channel('Ch. 15')
    d3 = scope.get_channel('Ch. 13')
    d4 = scope.get_channel('Ch. 11')
    d5 = scope.get_channel('Ch. 9')
    d6 = scope.get_channel('Ch. 8')
    d7 = scope.get_channel('Ch. 5')
    d8 = scope.get_channel('Ch. 4')
    d9 = scope.get_channel('Ch. 1')
    d10 = scope.get_channel('Ch. 2')

    f, axarr = plt.subplots(5, 2, sharex=True, figsize=(11.5,8))
    f.suptitle("Diode array ({})".format(shotnumber))
    #f.subplots_adjust(hspace=0)

    axarr[0, 0].plot(d1.time(), d1.signal())
    axarr[0, 0].grid()
    axarr[0, 0].set_ylabel(d1.verbose_name)

    axarr[0, 1].plot(d10.time(), d10.signal())
    axarr[0, 1].grid()
    axarr[0, 1].set_ylabel(d10.verbose_name)

    axarr[1, 0].plot(d2.time(), d2.signal()*2)
    axarr[1, 0].grid()
    axarr[1, 0].set_ylabel(d2.verbose_name)

    axarr[1, 1].plot(d9.time(), d9.signal())
    axarr[1, 1].grid()
    axarr[1, 1].set_ylabel(d9.verbose_name)

    axarr[2, 0].plot(d3.time(), d3.signal()*4)
    axarr[2, 0].grid()
    axarr[2, 0].set_ylabel(d3.verbose_name)

    axarr[2, 1].plot(d8.time(), d8.signal())
    axarr[2, 1].grid()
    axarr[2, 1].set_ylabel(d8.verbose_name)

    axarr[3, 0].plot(d4.time(), d4.signal()*4)
    axarr[3, 0].grid()
    axarr[3, 0].set_ylabel(d4.verbose_name)

    axarr[3, 1].plot(d7.time(), d7.signal())
    axarr[3, 1].grid()
    axarr[3, 1].set_ylabel(d7.verbose_name)

    axarr[4, 0].plot(d5.time(), d5.signal()*2)
    axarr[4, 0].grid()
    axarr[4, 0].set_ylabel(d5.verbose_name)

    axarr[4, 1].plot(d6.time(), d6.signal())
    axarr[4, 1].grid()
    axarr[4, 1].set_ylabel(d6.verbose_name)

    plt.xlim(-50, 300)


def analyze_array(path):
    _, TDS3 = cscopes.get_scope(path, 'TDS3', 'DIAG')
    shotnumber, tls4 = cscopes.get_scope(path, 'TLS4', 'DIAG')
    tls4.set_trigger_to_external_scope(TDS3)
    plot_scope(tls4, shotnumber)

if __name__ == '__main__':

    plt.ion()
    path = cscopes.prompt_for_path()
    analyze_array(path)
