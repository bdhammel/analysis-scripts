import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag/"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cmath
from core import cplot

SCOPENAME = "TDS12"
SCOPETYPE = "DIAG"
RAMP_CH = "Ch. 3"
TOF = 12

def stack_bdot_to_current(time, voltage):
    """Integrate the stack Bdot signal

    No calibration factor is know for the stack bdot. However, it is useful to 
    compare to for timing purposes

    Args:
        time (array)
        voltage (array)

    Returns:
        t (array)
        I (array)
        
    """
    voltage -= voltage[:200].mean()
    t, I = cmath.integrate_signal(time, voltage)
    return t, I/I.max()

def load_scope(path, scopename=SCOPENAME, scopetype=SCOPETYPE):

    scope = cscopes.import_scope_preferences(scopename + "_" + scopetype)
    shotnumber, raw_data = cscopes.read_scope_from_directory(path, scopename, scopetype)
    scope.load_data(raw_data)

    return scope

def get_streak_window(scope, ramp_ch=RAMP_CH):
    trigger = scope.get_channel(scope.trigger_channel)
    scope.set_trigger_ch(trigger, trigger_level=.5) # for the piece of shit scopes
    stack = scope.get_channel(scope.trigger_channel)
    streak = scope.get_channel(ramp_ch)
    time, current = stack_bdot_to_current(stack.time(), stack.signal())
    plt.figure('Streak Window', figsize=(5,3))
    plt.plot(time, current, label='current')
    plt.plot(streak.time(), streak.signal(), picker=5, label='streak')
    plt.xlim(-200,1800)
    plt.ylim(-.05, 1.2)
    plt.legend()
    plt.draw()


def load_streak_image(xmin, xmax, ymin=0, ymax=1):
    path = cscopes.prompt_for_path()
    img = cplot.load_image(path, xmin, xmax, ymin, ymax)
    return img

def convert_ramp_to_img_time(ramp, ramp_window=408.):
    """
        480 ns streak window is actually 408
    """
    y = ramp.get_ydata()
    ymid = np.where(y<(y.max()-y.min())/2)[0][-1]
    xmid = ramp.get_xdata()[ymid]-150
    x1 = xmid-ramp_window/2
    x2 = xmid+ramp_window/2
    ax = plt.gca()
    ax.axvspan(x1,x2,color='g',alpha=.3)
    plt.draw()
    return x1, x2

if __name__ == "__main__":
    plt.ion()
    print '''
    #==========================================================================#
    #   Get the timing of the streak window
    #==========================================================================#
    

    '''
    plt.close('all')
    plt.ion()
    mpl.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    mpl.rc('text', usetex=True)
    #mpl.rcParams['toolbar'] = 'None'

    path = cscopes.prompt_for_path()
    scope = load_scope(path)
    get_streak_window(scope)
    line = cplot.get_line_from_plot('streak')
    x1,x2 = convert_ramp_to_img_time(line)

    #window = WindowSelector(plt.gca(), line_name='streak')
    #plt.waitforbuttonpress()
    #print('after press')
    #raw_input()
    #x1, x2 = window.get_xvals()

    path = cscopes.prompt_for_path()
    img = cplot.load_image(path, xmin=np.round(x1), xmax=np.round(x2), ymin=-1, ymax=1)

    

