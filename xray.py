import os
import sys
import matplotlib.pyplot as plt

SAVE_PATH = "/Users/bdhammel/Documents/NTF/Analysis/2015_6_bs/quicklook/"  
CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag"

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

from core import cscopes
from core import cplot
from core import cmath
from analysis import xray_diag
from analysis import anode_bdot
from analysis import ekspla

import faraday_cups
import liquida
import dores
import diode_array
import streak_window

def load_scope(path, scopename, scopetype):
    TDS3 = cscopes.import_scope_preferences('TDS3_DIAG')
    shotnumber, tds3_raw_data = cscopes.read_scope_from_directory(path, 'TDS3', 'DIAG')
    TDS3.load_data(tds3_raw_data)

    scope = cscopes.import_scope_preferences(scopename + "_" + scopetype)
    shotnumber, raw_data = cscopes.read_scope_from_directory(path, scopename, scopetype)
    scope.load_data(raw_data)
    #scope.set_trigger_to_external_scope(TDS3)

    return scope


def stack_bdot_to_current(time, voltage):
    """Integrate the stack Bdot signal

    No calibration factor is know for the stack bdot. However, it is useful to 
    compare to for timing purposes

    Args:
        time (array)
        voltage (array)

    Returns:
        t (array)
        I (array)
        
    """
    voltage -= voltage[:200].mean()
    t, I = cmath.integrate_signal(time, voltage)
    return t, I/I.max()



def xrays(path, save_path):
    xray_diag.get_xrays(path)
    plt.savefig('{path}/5ch.png'.format(path=save_path))

    pcd = cplot.get_line_from_plot('PCD#6.4')
    pcd.set_color('b')

    sb = cplot.get_line_from_plot('stack')
    st, si = stack_bdot_to_current(sb.get_xdata(), sb.get_ydata())
    plt.close('all')

    tds10 = liquida.load_scope(path, 'TDS10', 'DIAG')
    liquida.load_additional_delays(tds10)
    liquida.plot_scope(tds10)
    sid = cplot.get_line_from_plot('Si')
    sid.set_color('b')
    la = cplot.get_line_from_plot('PMT1')
    la.set_label('Liquid A')
    plt.close('all')
    
    dores_scope = dores.dores(path)
    dores.graph_dores(dores_scope)
    dores_sig = cplot.get_line_from_plot('PMT1')
    dores_sig.set_label('DOReS')
    plt.grid()
    plt.close('all')

    fig = plt.figure('Xrays', figsize=(8.5,11))
    fig.subplots_adjust(hspace=0)

    ax1 = fig.add_subplot(511)
    plt.title('Xrays ({})'.format(shotnumber))
    line = ax1.plot(st, si)
    ax1.set_ylabel('Current')
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.grid()
    ax1.set_ylim(-.1,1.1)

    ax2 = fig.add_subplot(512, sharex=ax1)
    cplot.append_line(pcd)
    ax2.set_ylabel('PCD')
    plt.setp(ax2.get_xticklabels(), visible=False)
    plt.grid()
    ax2.legend_.remove()

    ax3 = fig.add_subplot(513, sharex=ax1)
    cplot.append_line(sid)
    ax3.set_ylabel('SiD')
    plt.setp(ax3.get_xticklabels(), visible=False)
    plt.grid()
    ax3.legend_.remove()

    ax4 = fig.add_subplot(514, sharex=ax1)
    cplot.append_line(la)
    ax4.legend_.remove()
    plt.setp(ax4.get_xticklabels(), visible=False)
    plt.grid()
    ax4.set_ylabel('Liquid A')

    ax5 = fig.add_subplot(515, sharex=ax1)
    cplot.append_line(dores_sig)
    ax5.legend_.remove()
    plt.grid()
    ax5.set_ylabel('DOReS')

    plt.xlim(-55,250)
    plt.draw()

    #plt.savefig('{path}/xray.png'.format(path=save_path))

if __name__ == "__main__":
    reload(xray_diag)
    reload(anode_bdot)
    reload(dores)
    reload(ekspla)
    reload(liquida)
    reload(faraday_cups)
    reload(diode_array)

    plt.ion()
    plt.close('all')
    path = cscopes.prompt_for_path()
    shotnumber = os.path.basename(path)

    save_path = SAVE_PATH + shotnumber
    if not os.path.exists(save_path):
        os.makedirs(save_path)


    print "\nloading save-to directory as: " + save_path

    xrays(path, save_path)
    #anode_bdot.get_current(path)
    plt.savefig("/Users/bdhammel/Desktop/{}.png".format(shotnumber))
    


