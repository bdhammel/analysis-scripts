import matplotlib.pyplot as plt
import sys

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag"

if CORE_DIAG not in sys.path:
    sys.path.append(CORE_DIAG )

from core import scopes as cscopes
from core import plot as cplot

dir = '/Users/bdhammel/Documents/NTF/Experiments/2014_9_Jet/Ekspla_pngs/'

plt.ion()

for i in range(3581, 3583): 
    plt.close('all')

    f, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex='col', sharey='row')

    #print 'enter shot number'
    shotnumber = str(i) #raw_input()

    path = dir + shotnumber + '_1s_ref.tif'
    img = plt.imread(path)
    imgplot = ax1.imshow(img)
    ax1.set_title('1s_ref')
    imgplot.set_cmap('gray')

    path = dir + shotnumber + '_1s.tif'
    img = plt.imread(path)
    imgplot = ax2.imshow(img)
    ax2.set_title('1s')
    imgplot.set_cmap('gray')

    path = dir + shotnumber + '_1d_ref.tif'
    img = plt.imread(path)
    imgplot = ax3.imshow(img)
    ax3.set_title('1d_ref')
    imgplot.set_cmap('gray')

    path = dir + shotnumber + '_1d.tif'
    img = plt.imread(path)
    imgplot = ax4.imshow(img)
    ax4.set_title('1d')
    imgplot.set_cmap('gray')

    plt.draw()

    f.savefig('/Users/bdhammel/Documents/NTF/Analysis/2014_9_Jet/ekspla/'+shotnumber)
