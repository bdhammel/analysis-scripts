import sys
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
#from PIL import Image
from scipy import ndimage

CORE_DIAG = "/Users/bdhammel/Documents/research_programming/core_diag/"
ANALYSIS_DIR = os.path.dirname(os.path.abspath(__file__))

if CORE_DIAG  not in sys.path:
    sys.path.append(CORE_DIAG )

if ANALYSIS_DIR not in sys.path:
    sys.path.append(ANALYSIS_DIR)

from core import cscopes
from core import cplot
from core.cplot import ImageData, IndexSelector
#from core.cmath import savitzky_golay

import streak_window

#==============================================================================#
#                           VISAR ANALYSIS 
#==============================================================================#

"""
    Author:     Ben Hammel
    Date:       3-2014
    Website:    http://www.thebrokendesk.com/post/analysis-of-a-2d-fringe-pattern/  

    Dependencies:
        * python 2.7
        * ipython       - necessary for interactive plotting 
        * numpy         - python numerical analysis package
        * matplotlib    - python plotting package
        * PIL           - used to import png image

    References:
        [1] P. Celliers. "Line-imaging velocimeter for shock diagnostics at the 
            omega laser facility." Review of Scientific Instruments, 75(11), 2004.
        [2]

    Example:

"""

#==============================================================================#
#   Globals
#==============================================================================#

SWEEP_SPEED = 480.
SLIT_WIDTH  =  1.5
SPACIAL_STEP = .2
ETALONS = {"1":71.5,
           "2":25.03,
           "3":9.97,
           "4":3.92}

#==============================================================================#
#   Event handling for Data Selectors 
#==============================================================================#

class DataSelector2D(object):
    """Select a 2d section of data on an image

    Allow user to draw a rectangle on the image
    information enclosed in the rectangle is returned to the user
    
    Rectangle is drawn by left clicking the mouse, and moving,
    releasing the left click sets the rectangle.
    when enter is pressed, disconnect the canvas

    Attributes
        _ax (pyplot axis): Axis the image is being displayed on
        _draw (boolean): Should the selector track mouse movement to draw the 
            rectangle in real time
        _rec (pyplot Rectangle): Artist object drawn on figure
        _color (str): color of rectangle g, r, b, etc...
        start_xy (tuple): starting coordinates of rectangle (upper left)
        width (int): width of rectangle
        height (int): height of rectangle

    """

    def __init__(self, img, color='r'):
        self._ax = plt.gca()
        self._draw = False
        self._rec = plt.Rectangle((0,0),0,0)
        self._color = color
        self.img = img

        # add initial rectangle to canvas
        self._ax.add_patch(self._rec)
        
        # initialize key-bindings and event handling
        self.cidpress = self._ax.figure.canvas.mpl_connect("button_press_event", self._mouse_press)
        self.cidrelease = self._ax.figure.canvas.mpl_connect("button_release_event", self._mouse_release)
        self.cidmove = self._ax.figure.canvas.mpl_connect("motion_notify_event", self._mouse_move)

    def disconnect(self):
        """Disconnect the canvas when "enter" is pressed
         set "waiting_for_entry" to False:
           used in main loop, so that the code and move to to next step
        def _key_press(self, event, force_disconnect=False):
        """
        self._ax.figure.canvas.mpl_disconnect(self.cidpress)
        self._ax.figure.canvas.mpl_disconnect(self.cidrelease)
        self._ax.figure.canvas.mpl_disconnect(self.cidmove)

    # record the starting place of the rectangle, enable the rectangle to be 'drawn'
    def _mouse_press(self, event):
        self.start_xy = [event.xdata, event.ydata]
        self._draw = True

    # draw the rectangle as the mouse is moved and record the location
    def _mouse_move(self, event):
        if self._draw and event.inaxes:
            self.width = event.xdata - self.start_xy[0]
            self.height = event.ydata - self.start_xy[1]
            self._draw_rec(self.start_xy, self.width, self.height)

    def _draw_rec(self, start, width, height):
        """Render the Rectangle to the figure
        """
        self._rec.remove()
        self._rec = plt.Rectangle(
                        (start[0], start[1]), 
                        width, 
                        height, 
                        fill = False, 
                        linewidth = 1.5,
                        color = self._color)
        self._ax.add_patch(self._rec)
        plt.draw()

    #   disable drawing, the user has chosen the rectangle
    def _mouse_release(self, event):
        self._draw = False

    def data(self):
        """return the data enclosed by the rectangle
        """
        row_start = self.start_xy[0]
        row_end = self.start_xy[0]+self.width

        # selector starts high and goes low
        col_end = self.start_xy[1] 
        col_start = self.start_xy[1]+self.height

        data = self.img.as_xy()

        xmin, ymin = self.img.pixel_to_physical(row_start,col_start)
        xmax,  ymax = self.img.pixel_to_physical(row_end, col_end)

        print('''
        starting xpixel: {row_start} time value: {xmin}
        starting ypixel: {col_start} spacial value: {ymin}
        ending xpixel: {row_end} time value: {xmax}
        ending ypixel: {col_end} spacial value: {ymax}'''.format(
                                                    row_start=row_start,
                                                    xmin=xmin,
                                                    col_start=col_start,
                                                    ymin=ymin,
                                                    row_end=row_end,
                                                    xmax=xmax,
                                                    col_end=col_end,
                                                    ymax=ymax))

        #   get the image back from the axis
        working_image = ImageData(np.transpose(
                                                data[col_start:col_end,
                                                    row_start:row_end]),
                                                xmin=xmin,
                                                xmax=xmax,
                                                ymin=ymin,
                                                ymax=ymax)
        return working_image

    def get_ax(self):
        return self._ax

class LockedSelector(DataSelector2D):
    """Select a section of data with specific constraints

    Allows user to draw a rectangle on the data image under specific constraints
    i.e. only draw a rectangle starting at (x,y)
         only draw a rectangle of width (or height) ____
    """


    def __init__(self, img, lock_start=False, lock_width=False, lock_height=False):
        DataSelector2D.__init__(self, img, 'g')

        self.lock_start = lock_start
        self.lock_width = lock_width
        self.lock_height = lock_height
                        

    # record the starting place of the rectangle, enable the rectangle to be 'drawn'
    def _mouse_press(self, event):
        if self.lock_start:
            self.start_xy = self.lock_start
        else:
            self.start_xy = [event.xdata, event.ydata]

        self._draw = True

    # draw the rectangle as the mouse is moved and record the location
    def _mouse_move(self, event):
        if self._draw and event.inaxes:
            if self.lock_width:
                self.width = self.lock_width
            else:
                self.width = event.xdata - self.start_xy[0]

            if self.lock_height:
                self.height = self.lock_height
            else:
                self.height = event.ydata - self.start_xy[1]

            self._draw_rec(self.start_xy, self.width, self.height)

    # return a selection without user input 
    def force_return(self):
        self.start_xy = self.lock_start
        self.width = self.lock_width
        self.height = self.lock_height

        DataSelector2D.disconnect(self)

        return DataSelector2D.data(self)

#----------------------------------------------------------------------#
#  holder class for spectral image data
#----------------------------------------------------------------------#
class SpectrogramData(ImageData):

    def __init__(self, spec, wavenumbers, *args, **kwargs):
        ImageData.__init__(self, spec, *args, **kwargs)
        self.wavenumbers = wavenumbers
        self.start_frequency = None
        self.end_frequency = None

    # Save the frequencies that were filtered
    def filtered_frequencies(self, start, end):
        self.start_frequency = start
        self.end_frequency = end

    @property
    def y_range(self):
        """y_range (array): spacial array for y labels 
        """
        dy = (self.ymax-self.ymin)/100
        return np.around(np.arange(self.ymin, self.ymax, dy),2)

    # rearrange the y values so the frequencies are plotted in a intuitive way
    # -N/2 ... N/2 range 
    def readable(self):
        return np.transpose([make_fft_readable(self.wavenumbers, row) for row in self.as_yx()])

    def show(self, title, color= 'hot'):

        readable_spec = self.readable()
        
        #shape = readable_spec.shape
        fig = plt.figure(title, dpi = 100, figsize=(6, 4))
        fig.set_tight_layout(True)
        ax = fig.add_subplot(111)
        implot = ax.imshow(readable_spec, aspect='auto')
        implot.set_cmap(color)

        ax.set_xticks(self.xtick_locations())
        ax.set_xticklabels(self.xtick_labels())
        ax.set_xlabel("Time [ns]")
        ax.set_xlim(0,self.xpixels)

        ax.set_yticks(self.ytick_locations())
        ax.set_yticklabels(self.ytick_labels())
        ax.set_ylim(self.ypixels/2-25, self.ypixels/2+25)
        ax.set_ylabel("wavenumber $10^3 m^{-1}$")

        plt.draw()



#==============================================================================#
#  Smoothing 
#==============================================================================#

def smooth(velocity_map, window):
    """Smooth unwanted features using a Gaussian filter
    
    """
    temp = ndimage.gaussian_filter(velocity_map.as_yx(), window)

    fig = plt.gcf()
    fig.clear()

    velocity_map.data = temp

    return velocity_map

#==============================================================================#
#   Plotting functions
#==============================================================================#

def plot_velocity(time, velocity, picker=False):

    fig =  plt.figure("Velocity")
    fig.clear()

    if picker:
        plt.plot(time, velocity, picker=2)
    else: 
        plt.plot(time, velocity)

    plt.xlabel("Time [ns]")
    plt.ylabel("Velocity $[km \cdot s^{-1}]$")


#==============================================================================#
#   Data importing
#==============================================================================#

def select_working_data(img):
    """Return the section of data the user wants to work with
 
    Return full section of working data and sub section for constant fringes
    to be used as a reference in FFT analysis
    """
    data_selector = DataSelector2D(img)

    # hold program until the user has selected the data and pressed "enter"
    raw_input("...waiting ")
    data_selector.disconnect()


    print "Select constant fringes for FFT reference"
    refrence_selector = LockedSelector(img, 
                                lock_start=data_selector.start_xy,
                                lock_height=data_selector.height)
    raw_input("...waiting ")
    refrence_selector.disconnect()
    
    return data_selector, refrence_selector


#==============================================================================#
#   Fourier Transform Functions
#==============================================================================#

def get_wavenumber(img, spec):
    N = len(spec[0])
    width = img.ymax - img.ymin
    width = np.linspace(0, width, N)
    step_size = width[1]-width[0]

    ##hz = [0, 1, ..., N/2-1, -N/2, ..., -1] / (d*N)         if N is even
    ##hz = [0, 1, ..., (N-1)/2, -(N-1)/2, ..., -1] / (d*N)   if N is odd
    return np.fft.fftfreq(N, d=step_size)

def make_fft_readable(wave_nums, f):
    zipped = zip(wave_nums, f)
    zipped.sort()
    _wave_num, f = map(list, zip(*zipped))
    return abs(np.array(f))

def spectrogram(img):
    """Generate a spectrogram from an image 

    Do an FFT for each point in time 
    Data is stored in the manner returned by np.fft.fft ( hz = 0->N/2 -N/2 -> 0 )
     - this is necissary to do the inverse fft.
    however, it's plotted as -N/2 -> N/2
    """
 
    spec = np.array([np.fft.fft(row) for row in img.as_yx()])
    wavenumbers = get_wavenumber(img, spec)

    spec = SpectrogramData(
                        spec,
                        wavenumbers=wavenumbers,
                        xmin=img.xmin,
                        xmax=img.xmax,
                        ymin=wavenumbers.min(),
                        ymax=wavenumbers.max())

    return spec

def filter_spectrogram(spec, average_to_index=None, start_frequency=None, end_frequency=None):
    """Remove unwanted frequencies by setting values in the spectrogram to 0

    Args:
        average_to_index (float): Width of the user's selection of constant frindges
        start_frequency (float): the starting frequency to keep (passed during analysis of background)
        end_frequency (float): the last frequency to keep (passed during analysis of background)
    """

    # Takes the first part of the image (constant fringes) and averages the FFT
    # lets the user select the starting and ending frequencies they want to keep
    if average_to_index:
        avg_fft(spec, average_to_index)

        print "Select frequencies of interest"
        index_selector = cplot.DataSelector1D()
        raw_input("...waiting ")
        index_selector.disconnect()

        i1, i2 = index_selector.get_indices() 
        start_frequency = spec.wavenumbers[i1]
        end_frequency = spec.wavenumbers[i2] 

    # Get all of the indices of the wave number array between the points selected
    # by the user
    filtered_spec = [] 
    filter_indices = np.where( (spec.wavenumbers > start_frequency) & (spec.wavenumbers < end_frequency) )
        
    #   Create an array the length of the wave number array filled with zeros
    #   then fill only the indices that were selected in the filter
    for row in spec.as_yx():
        filtered_row = np.zeros_like(row)
        filtered_row[filter_indices] = row[filter_indices]
        filtered_spec.append(filtered_row)
     
    filtered_spec = SpectrogramData(
                            np.array(filtered_spec),
                            wavenumbers=spec.wavenumbers,
                            xmin=spec.xmin,
                            xmax=spec.xmax,
                            ymin=spec.ymin,
                            ymax=spec.ymax)

    filtered_spec.filtered_frequencies(start_frequency, end_frequency)
    print filtered_spec.start_frequency, filtered_spec.end_frequency

    return filtered_spec


def avg_fft(spec, average_to_index):
    """Construct a FFT from the area selected by the user of constant fringes.
    Average this for picking filtered frequencies.
    """

    f = np.array([ np.average(row[:average_to_index]) for row in spec.as_xy() ])
    f = abs(f)
    f/=f.max()

    fig = plt.figure('FFT',figsize=(5,3))
    ax = fig.add_subplot(111)
    ax.plot(spec.wavenumbers[:75], f[:75], picker=2)
    ax.set_ylim(-0.05,)
    ax.set_xlabel("wavenumber $10^3 m^{-1}$")
    plt.draw()
    plt.tight_layout()

#----------------------------------------------------------------------#
#   Preform an inverse fft of each row of the spectrogram
#----------------------------------------------------------------------#
#   recreate the image with the filter applied
def do_inv_fft(spec, img):

    # both real and imaginary parts are needed to do the phase unwrap
    filtered_iimg = np.array([ np.fft.ifft(row).imag for row in spec.as_yx() ])
    filtered_img  = np.array([ np.fft.ifft(row).real for row in spec.as_yx() ])

    filtered_img  = ImageData(filtered_img, 
                            xmin=img.xmin,
                            xmax=img.xmax,
                            ymin=img.ymin,
                            ymax=img.ymax)
    filtered_iimg = ImageData(filtered_iimg,
                            xmin=img.xmin,
                            xmax=img.xmax,
                            ymin=img.ymin,
                            ymax=img.ymax)

    return filtered_img, filtered_iimg

#----------------------------------------------------------------------#
#   Get the wrapped phase 
#----------------------------------------------------------------------#
#
#   W(x) = archtan( sin(x) / cos(x) )
#   img is the real image
#   iimg is the imaginary image 
#
def get_phase(img, iimg):
    phase = np.arctan(iimg.as_yx()/img.as_yx())
    phase = ImageData(phase,
                        xmin=img.xmin,
                        xmax=img.xmax,
                        ymin=img.ymin,
                        ymax=img.ymax)
    return phase 

#==============================================================================#
#   Background Subtraction
#==============================================================================#

def background_velocity(raw_img, raw_selector, raw_spec, vpf):
    """Construct a background velocity map from a reference image

    Args:
        raw_img (img): Origional image loaded into program, contains correct min and max values
        raw_selector (img): parameters used to select working data from raw_image. 
            Takes the same section of the refrence image
        raw_spec (img): contains the info for the filtered frequencies
        vpf (float): convert the phase map into a velocity map

    Returns:
        bg_velocity (img): velocity map of reference image
    """
    print "\nUpload background image"
    path = cscopes.prompt_for_path()
    img = plt.imread(path)

    img = ImageData(img,
                    xmin=raw_img.xmin,
                    xmax=raw_img.xmax,
                    ymin=raw_img.ymin,
                    ymax=raw_img.ymax)

    img = LockedSelector(img,
                         raw_selector.start_xy,
                         raw_selector.width,
                         raw_selector.height)

    img = img.force_return()

    img.show("background")

    spec = spectrogram(img)

    filtered_spec = filter_spectrogram(spec, 
                                       start_frequency=raw_spec.start_frequency,
                                       end_frequency=raw_spec.end_frequency)

    filtered_img, filtered_iimg = do_inv_fft(filtered_spec, img)
    bg_phase = get_phase(filtered_img, filtered_iimg)

    bg_velocity = get_velocity_map(bg_phase, vpf)

    return bg_velocity

def subtract_background(raw_velocity, background_velocity):
    """Subtract the background velocity map from the velocity map of the data
    """
    velocity = raw_velocity.as_yx() - background_velocity.as_yx()
    velocity = ImageData(velocity,
                    xmin=raw_velocity.xmin,
                    xmax=raw_velocity.xmax,
                    ymin=raw_velocity.ymin,
                    ymax=raw_velocity.ymax)

    return velocity



#==============================================================================#
#  velocity
#==============================================================================#

#----------------------------------------------------------------------#
#   Calculate the velocity per fringe 
#----------------------------------------------------------------------#
# eqn (2) from [1]
#
def vpf_from_etalon():
    _wavelength = 532.0*10**(-9)
    _delta = 0.0318
    _n = 1.4585
    _c = 299792458.0

    print "Pick an etalon thickness:"
    print '''
            [1].... 71.5
            [2].... 25.03
            [3]....  9.97
            [4]....  3.92
          '''

    choice = raw_input("> ")

    h = ETALONS[choice]

    tau = 2 * h / _c * (_n - 1/_n)
    vpf_0 = _wavelength / ( 2 * tau * ( 1 + _delta))

    return vpf_0

def get_velocity_map(img, vpf):
    """Construct velocity map from phase map using VPF

    Args:

    Attributes:
        _threshold = +/- pi/2 to account for noise
        _max_dphase = min possible shift in phase to be considered a jump
        _min_dphase

    Returns:
    """

    _threshold = .07
    _max_dphase = np.pi/2. - _threshold
    _min_dphase = -1 * _max_dphase

    velocity_img = []
    
    # Generate velocity for each spacial position
    for j, row in enumerate(img.as_xy()):
        velocity_row = []
        v = 0.

        # add up relative velocity for each spacial position
        # if there is a jump of more than pi, 

        for i in range(1,len(row)):
            dphase = row[i] - row[i-1] 
            if dphase < _min_dphase:
                dphase = 0 # 2*np.pi shift
            elif dphase > _max_dphase:
                dphase = 0 # 2*np.pi shift

            v += dphase * vpf / (2*np.pi)
            velocity_row.append(v)

        velocity_img.append(velocity_row)

    velocity_img = np.array(velocity_img)
    velocity_img = ImageData(
                            np.transpose(velocity_img),
                            xmin=img.xmin,
                            xmax=img.xmax,
                            ymin=img.ymin,
                            ymax=img.ymax)

    return velocity_img

def get_velocity(velocity_map, lineout_width=-25):
    """Take a lineout of the velocity map 

    let the user select a section of the velocity map.
    average the values for each temporal point, and return a 1d array.
    construct the time values based on the ratio used in the image properties.

    Args:
        velocity_map (img): 2D phase map of velocity
        lineout_width (negative int): width (technically height) of lineout used
            to select average of velocity. number needs to be negitive to be consistant 
            with the way in which a user selects 2D data (starting high and goes low). 
            If not negative, data extraction will fail
    """


    lineout = LockedSelector(velocity_map, lock_height=lineout_width)

    print "\nSelect a line out. width: {}".format(lineout_width)
    raw_input("waiting... ")

    lineout.disconnect()
    
    data = lineout.data()

    velocity = np.array([np.average(row) for row in data.as_yx()])

    min_time = velocity_map.xmin
    max_time = velocity_map.xmax
    time = np.linspace(min_time, max_time, len(velocity))

    return time, velocity 

#----------------------------------------------------------------------#
#   add a full fringe shift to user selected point
#----------------------------------------------------------------------#
def add_fringe_shift(time, velocity, vpf):

    # let user select discontinuity
    picker = IndexSelector(plt.gca())

    raw_input()
    picker.disconnect()
    index = picker.get_index()

    print "index selected was: ", index

    # add a full fringe shift, and remove some data points before the shift (these
    # would have been generated incorrectly during the analysis)
    print "up or down?"
    choice = raw_input().lower()
    if choice[0] == 'u':
        velocity[index:] += vpf
    else:
        velocity[index:] -= vpf
    delete_i = np.arange(index-10,index)
    velocity = np.delete(velocity, delete_i)
    time = np.delete(time, delete_i)

    # remove old velocity trace and replot
    ax = plt.gca()
    ax.lines.pop()
    ax.plot(time, velocity)

    return time, velocity

#==============================================================================#
#   Main run Function
#==============================================================================#
if __name__ == "__main__":

    # initialise the plotting parameters 
    plt.ion()
    #mpl.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    #mpl.rc('text', usetex=True)
    #mpl.rc('font', size=18)
    #mpl.rcParams['toolbar'] = 'None'
    plt.close('all')

    #load raw data
    print "\nLoad Scope file with +ramp. Parameters for analysis set in `streak_window.py`"
    path = cscopes.prompt_for_path()
    scope = streak_window.load_scope(path)
    streak_window.get_streak_window(scope)
    line = cplot.get_line_from_plot('streak')
    x1,x2 = streak_window.convert_ramp_to_img_time(line)

    print "\nLoad png Streak image"
    path = cscopes.prompt_for_path()
    raw_img = cplot.load_image(
                            path, xmin=np.round(x1), xmax=np.round(x2), 
                            ymin=-.5, ymax=.5, transpose=False)

    # crop out the data the user wants to work with 
    print "Select Working data from raw image."
    data_selector, refrence_selector = select_working_data(raw_img)
    img = data_selector.data()
    img.show("Cropped Data")

    # Make a spectrogram of the data of the data and apply the filter 
    spec = spectrogram(img)
    spec.show("Spectrogram")
    filtered_spec = filter_spectrogram(
                                spec,
                                average_to_index = refrence_selector.width
                                )
    filtered_spec.show('Filtered Spectrogram')

    # Invert the FFT to create the filtered image
    filtered_img, filtered_iimg = do_inv_fft(filtered_spec, img)
    filtered_img.show('Filtered')

    # extract the phase from the arctan of the real and imag components 
    # to generate the phase image
    phase = get_phase(filtered_img, filtered_iimg)
    phase.show("Wrapped Phase")

    # get the etalon used from the user
    vpf = vpf_from_etalon()

    # follow each spacial component to generate a velocity map.
    raw_velocity_map = get_velocity_map(phase, vpf)

    # repeat the previous steps, with the same parameters, on the reference image
    # use this to subtract the background if user wants to
    print "\nSubtract a background? (y or n)"
    choice = raw_input()
    if choice.lower().startswith('y'):
        bg_velocity_map = background_velocity(raw_img, data_selector, filtered_spec, vpf)
        velocity_map = subtract_background(raw_velocity_map, bg_velocity_map)
    else:
        velocity_map = raw_velocity_map

    raw_velocity_map.show(title = "Velocity Map", color='jet')

    # smooth the velocity to get out any unwanted variation
    smooth_data = True
    while True:
        print "Smooth data? (yes or no)"
        smooth_data = raw_input().lower()
        if smooth_data[0] == "n":
            break
        elif smooth_data[0] == "y":
            print "Select a window size (i.e. ~2)"
            window = int(raw_input().lower())
            velocity_map = smooth(velocity_map, window)
            velocity_map.show(title="Velocity Map", color='jet')


    # Out put the final velocity map, let the user select a line out
    time, velocity = get_velocity(velocity_map)
    plot_velocity(time, velocity)

    # let user add in a fringe jump if there is a discontinuity
    print "add 2 pi shift?"
    shift = raw_input().lower()
    
    if shift[0] == 'y':
        plot_velocity(time, velocity, picker = True)
        time, velocity = add_fringe_shift(time, velocity, vpf)
        plot_velocity(time, velocity)

    print "Invert?"
    choice = raw_input().lower()
    if choice[0] =='y':
        line = plt.gca().lines[0]
        line.remove()
        plot_velocity(line.get_xdata(), line.get_ydata()*-1)
        plt.draw()


    ax = plt.gca()
    line = ax.lines[0]
    """
    plt.gcf().canvas.mpl_connect('pick_event', cplot.DataCursor(plt.gca()))
    line.set_picker(5)
    """

    print "Save?"
    save = raw_input().lower()
    if save[0] == 'y':
        fig = plt.figure("Velocity")
        path = cscopes.prompt_for_path()
        fig.savefig(path + '/velocity.png')




