import sys
import os
import re
import matplotlib.pyplot as plt
import numpy as np


class TallyEnergyCard(object):
    
    def __init__(self):
        self._energies = []
        self._fluence = []


class F5Detector(object):
    """F5 detector object
    """

    def __init__(self, location):
        self._location = location
        self._fluence = None

    @property
    def location(self):
        return self._location

    @property
    def fluence(self):
        return self._fluence

    @property
    def label(self):
        location_floats = list(map(float, self._location))
        location = list(map(str, location_floats))
        return '<' + ', '.join(location) + '>'

    def set_fluence(self, fluence):
        """
        fluence is the flux integrated over time
        """
        self._fluence = float(fluence)

class F5Tally(object):

    __type__ = 'f5'

    _process_tally_dispatcher = {
            "_add_new_detector":"\s*?detector located at x,y,z = ([0-9.E+]+) ([0-9.E+]+) ([0-9.E+]+)",
            "_set_detector_fluence":"^\s{17}([0-9.E-]+) [0-0]\.[0-9]{4}",
            "_apply_energy_card":"^\s{6}energy",
            }

    def __init__(self):
        self.detectors = []

    def process(self, line):
        """Parse each line sent and extract important info depending on the 
        regex match in _process_tally_dispatcher
        """
        for processor, regex in self._process_tally_dispatcher.items():
            m = re.match(regex, line)
            if m:
                getattr(self, processor)(*m.groups())

    def _add_new_detector(self, x_pos, y_pos, z_pos, *args, **kwargs):
        """Add a new detector to the Tally based on the location passed

        If the location passed is the same as the previous detector added, then 
        it is containing information on the collided photon flux. Ignore this
        information, and do not create a detector. 
        """
        location = (x_pos, y_pos, z_pos)
        try:
            previous_detector = self.detectors[-1]
        except IndexError:
            self.detectors.append(F5Detector(location))
        else:
            if location != previous_detector.location:
                self.detectors.append(F5Detector(location))

    def _apply_energy_card(self):
        pass

    def _set_detector_fluence(self, fluence, *args, **kwargs):
        """Set the fluence recorded at the detector

        Upon creation of a detector, fluence is set to None. If no fluence, set
        the value.
        If a fluence already exists for a detector, information is referring to the 
        uncollided photon flux, ignore this. 
        """
        detector = self.detectors[-1]
        if not detector.fluence:
            detector.set_fluence(fluence) 

    def plot_detector_response(self):
        fluences = []
        labels = []
        for detector in self.detectors:
            fluences.append(detector.fluence)
            labels.append(detector.label)

        y_pos = np.arange(len(labels))
        plt.barh(y_pos, fluences, align='center')
        plt.yticks(y_pos, labels)
        plt.xlabel('Fluence (counts/source particle)')
        plt.show()
        plt.draw()
        plt.tight_layout()



class MCNPOutput(object):
    """Take an MCNP output file and parse it into python objects for 
    post-processing
    """
                    
    _process_block_dispatcher = {
                'tally':'_load_tally'
            }

    _tally_dispatcher = {
                '5':'_load_f5tally',
            }

    def __init__(self, file_path=None):
        self._tallies = []
        self.file_path = file_path
        self._load_mcnp_file()

    def _load_mcnp_file(self):
        """Load the mcnp file and process it

        For each line in the file check if it should start a processor. If the 
        line queues a processor, call the appropriate function given in the 
        dispatcher. 
        """

        if self.file_path:
            f =  open(self.file_path, 'r')
            mcnp_output_file = f
        else:
            mcnp_output_file = sys.stdin

        for line in mcnp_output_file:
            block_processor = self.get_processor_from_line(line)
            if block_processor:
                getattr(self, block_processor)(file=f, current_line=line)

        try:
            f.close()
        except None:
            pass

    def _load_tally(self, file, current_line, *args, **kwargs):
        """Start Processor for the 1tally keyword

        Get tally type from line, and call the appropriate processor for that
        tally type. Save tally object
        """
        match = re.match('[0-9]tally\s+([0-9])', current_line)
        if match:
            tally_processor = self._tally_dispatcher[match.group(1)]
            tally = getattr(self, tally_processor)(file)
            self._tallies.append(tally)

    def _load_f5tally(self, file):
        """F5 tally type processor

        Send each line in block for a F5 tally type object to process. When 
        a new keyword is encountered, the block is completed, return the tally 
        object
        """
        f5tally = F5Tally()
        for line in file:
            if self.get_keyword_from_line(line):
                return f5tally
            else:
                f5tally.process(line)

    def get_keyword_from_line(self, line):
        """Search the given line for a keyword

        Keywords start with a number as the first charictor
        e.g. 1tally, 1analysis, 1problem
        """
        keyword_matches = re.match('[0-9]([a-z]+)', line) 
        if keyword_matches:
            return keyword_matches.group(1)
        else:
            return None

    def get_processor_from_line(self, line):
        """Check to see if a line containing a keyword should queue a processor

        If keyword corresponds to a processor, return the name of the function
        call
        """
        keyword = self.get_keyword_from_line(line)
        if keyword:
            processor = self._process_block_dispatcher.get(keyword)
            if processor:
                return processor
            else:
                return None


if __name__ == "__main__":
    plt.ion()
    plt.close('all')

