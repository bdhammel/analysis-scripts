import sys
import os
import re
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

plt.ion()

record = False
times = []
data = {}

#_file = raw_input('> ')
#_file = '/Users/bdhammel/Dropbox/mcnp/spherical'
_file = '/Users/bdhammel/Dropbox/mcnp/full_src'
#_file = '/Users/bdhammel/Dropbox/mcnp/det_resp'
with open(_file.strip()) as f:
    stop = False
    for line in f:
        start = re.match('[0-9]tally\s+[0-9]+', line)
        if start:
            record = True
        elif stop:
            break
        elif record:
            time = re.match('^\s*time:\s+((?:[0-9.E+]+\s*)*)', line)
            if time:
                times.extend(list(map(float, time.groups()[0].split())))
            else: 
                talies = re.match('^\s+((?:[0-9.E+-]+\s*)*)', line)
                if talies:
                    items = talies.groups()[0].split()
                    try:
                        energy, tallies = float(items[0]), items[1::2]
                        if len(tallies)>1:
                            try:
                                data[energy].extend(list(map(float,tallies)))
                            except KeyError:
                                data[energy] = list(map(float,tallies))
                    except IndexError:
                        pass
            stop = re.match('^\S', line)


#plt.close('all')
energy = []
r = []
p = None
x = 0

for key in sorted(map(float,data.keys())):
    energy.append(float(key))
    value = data[key]
    x+=1
    if p is not None:
        p = np.vstack((p,value[:-1]))
    else:
        p = value[:-1]

tt, ee = np.meshgrid(times, energy, indexing='xy')
tt*=10
fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(tt, ee, p, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
ax.set_xlabel('Time[ns]')
ax.set_ylabel('Energy [MeV]')

'''
plt.figure()
r = np.array([sum(np.multiply(row,energy)) for i, row in enumerate(p.T)])
plt.plot(np.array(times)*10, r*-1)
'''

s = []
for t in range(len(times)):
    y=np.array([])
    for e in range(len(energy)):
        y = np.append(y, p[e,t]*energy[e])
    s.append(y.sum())

plt.figure()
plt.plot(times, s)
